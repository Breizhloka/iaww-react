import React from 'react';
import classes from'./Table.module.css';

import Score from '../Score/Score.js';

const table = (props) => {
  let nbLigne = props.nbLigne;
  const scoreList = [];

  for (var i = 0; i < nbLigne; i++) {
    scoreList.push(<Score key= {i}/>);
  };

  return (
    <div className= {classes.Table}>
      <form>
        <table>
          <thead>
            <tr>
              <th>Joueur</th>
              <th>Points de victoire</th>
              <th>Noir</th>
              <th>Jaune</th>
              <th>Vert</th>
              <th>Bleu</th>
              <th>Jetons rouges</th>
              <th>Jetons bleus</th>
              <th>Total</th>
            </tr>
          </thead>
            {scoreList}
        </table>
        <input type="button" name="aj_li" value="Ajouter une ligne" onClick={props.click} />
      </form>
    </div>
)
};

export default table;

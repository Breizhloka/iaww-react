import React from 'react';
import classes from'./Score.module.css';

import Multiplicateur from '../Multiplicateur/Multiplicateur.js';


const score = (props) => {
  const ligne = (
    <tr className= {classes.Score}>
      <td><input type="text" name="nom_joueur" placeholder="Joueur" /></td>
      <td><input type="number" name="pts_vict" placeholder="Nb pts victoire" /></td>
      <Multiplicateur type="cartes" couleur="noires"/>
      <Multiplicateur type="cartes" couleur="jaunes"/>
      <Multiplicateur type="cartes" couleur="vertes"/>
      <Multiplicateur type="cartes" couleur="bleues"/>
      <Multiplicateur type="jetons" couleur="rouges"/>
      <Multiplicateur type="jetons" couleur="bleus"/>
      <td><input type="number" disabled /></td>
    </tr>
  );

  return (
    <tbody>
      {ligne}
    </tbody>
)
};

export default score;

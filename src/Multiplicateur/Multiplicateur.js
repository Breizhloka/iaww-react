import React from 'react';
import classes from'./Multiplicateur.module.css';

const multiplicateur = (props) => {
  return (
      <td>
        <input type="number" name={"pts_" + props.type + "_" + props.couleur} placeholder={"Nb " + props.type} /> <br />x<br />
        <input type="number" name={"mult_"+ props.type + "_" + props.couleur} placeholder="Multiplicateur" />
     </td>
)
};

export default multiplicateur;

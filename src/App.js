import React, { Component } from 'react';
import classes from'./App.module.css';
import Table from './Table/Table.js';


class App extends Component {
  state = {
    nbLigne: 1
  }

  cloneHandler = () => {
    this.setState({nbLigne: this.state.nbLigne + 1})
  }

  render() {
    return (
      <div className= {classes.App}>
        <Table click= {this.cloneHandler} nbLigne= {this.state.nbLigne} />
      </div>
    );
  }
}

export default App;
